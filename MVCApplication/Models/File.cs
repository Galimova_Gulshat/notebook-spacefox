using Microsoft.AspNetCore.Http;

namespace MVCApplication.Models
{
    public class File
    {
        public int Id { get; set; }
        
        public string Title { get; set; }
        public string Name { get; set; }
        public string Path { get; set; }
        
        public string UserEmail { get; set; }
        
        public bool IsPrivate { get; set; }
        
        public string FormFile { get; set; }
        
        public string ContentType { get; set; }
    }
}