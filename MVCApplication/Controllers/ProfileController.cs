using System;
using System.IO;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MVCApplication.Data;
using File = MVCApplication.Models.File;

namespace MVCApplication.Controllers
{
    public class ProfileController : Controller
    {
        private ApplicationContext _context;

        public ProfileController(ApplicationContext context)
        {
            _context = context;
        }
        // GET
        public IActionResult Index()
        {
            return
            View(_context.Files.Where(f => !f.IsPrivate));
        }
        
        public IActionResult Account()
        {
            return View(_context.Files.Where(f => f.UserEmail == User.Identity.Name));
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(string name, IFormFile formFile, string isPrivate)
        {
            var file = new File();
            var privateBool = isPrivate != null && !isPrivate.Equals("public");
            file.IsPrivate = privateBool;
            file.Title = name;
            file.UserEmail = User.Identity.Name;
            if (formFile != null && formFile.Length > 0)
            {
                var fileName = formFile.FileName;
                var extension = Path.GetExtension(fileName);
                    var filePath = Path.GetFullPath(Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/Files"));
                    using (var fileStream = new FileStream(Path.Combine(filePath, fileName), FileMode.Create))
                    {
                        formFile.CopyTo(fileStream);
                    }
                    file.Path = "wwwroot/Files/"+ fileName;
                    file.Name = formFile.Name;
                    file.ContentType = formFile.ContentType;
                    var readStream = formFile.OpenReadStream();
                    byte[] bytes = new byte[readStream.Length];                
                    readStream.Read(bytes, 0, Convert.ToInt32(readStream.Length));
                    var strBase64 = Convert.ToBase64String(bytes);
                    var content = string.Format("data:" + formFile.ContentType + ";base64,{0}", strBase64);
                    file.FormFile = content;
            }

            _context.Files.Add(file);
            _context.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}