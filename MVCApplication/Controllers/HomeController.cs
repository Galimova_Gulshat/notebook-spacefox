﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using MVCApplication.Data;
using MVCApplication.Models;

namespace MVCApplication.Controllers
{
    public class HomeController : Controller
    {
        private readonly IHostingEnvironment _appEnvironment;
        private readonly ApplicationContext _context;

        public HomeController(ApplicationContext context, IHostingEnvironment appEnvironment)
        {
            _context = context;
            _appEnvironment = appEnvironment;
        }

        public IActionResult Index()
        {
            return View();
        }
        
        public IActionResult GetFile(int id)
        {
            var file = _context.Files.FirstOrDefault(f => f.Id == id);
            string file_path = Path.Combine(_appEnvironment.ContentRootPath, file.Path);
            string file_type = file.ContentType;
            return PhysicalFile(file_path, file_type, file.Title);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        public IActionResult AddUser()
        {
            _context.Users.Add(new User {FirstName = "Gulshat", LastName = "Galimova"});
            _context.SaveChanges();
            return View("Index");
        }

        public IActionResult ShowUsers()
        {
            return View(_context.Users);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
